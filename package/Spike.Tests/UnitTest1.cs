using NUnit.Framework;
using Stackage.Core.Abstractions;

namespace Tests
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            var subject = new Class1();

            Assert.That(subject.Value, Is.EqualTo("Default"));
        }
    }
}
